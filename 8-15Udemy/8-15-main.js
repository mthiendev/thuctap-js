// nếu là số dương
function checkNumber1(n) {
  if (n > 0) {
  }
}
// nếu là số dương chữn
function checkNumber2(n) {
  if (n > 0 && n % 2 === 0) {
  }
}
checkNumber2(4);
// nếu số dương chẵn là lớn hưn 10
function checkNumber3(n) {
  if (n > 10 && n % 2 === 0) {
    console.log(`${n} là số chẵn`);
  }
}
checkNumber3(12);
// nếu số duowgn chẵn hoặc số âm lẽ
function checkNumber4(n) {
  if ((n > 0 && n % 2 === 0) || (n < 0 && n % 2 != 0)) {
    console.log("ok");
  }
}
checkNumber4(-3);
function getTicketPrice(age) {
  if (age <= 0 || age > 125) return -1;
  if (age < 6 || age >= 70) {
    return 0;
  }
  if (age >= 6 && age <= 12) {
    return 20;
  }
  return 50;
}
function randomNumber(n) {
  if (n <= 0) return -1;
  const random = Math.random() * n;
  const result = Math.round(random);
  return result;
}
console.log(randomNumber(3));
console.log(randomNumber(3));
console.log(randomNumber(3));
console.log(randomNumber(3));
console.log(randomNumber(3));
console.log(randomNumber(3));
console.log(randomNumber(3));
function randomNumberInRange(a, b) {
  if (a >= b) return -1;
  const random = Math.random() * (b - a);
  return Math.round(random);
}
console.log("randomNumberInRange: ", randomNumberInRange(4, 7));

function convertHoursToSeconds(hours) {
  const SECONDS_PER_HOUR = 3600;
  if (hours <= 0) return -1;
  if (hours === 0) return 0;
  return hours * SECONDS_PER_HOUR;
}
function findMax(a, b, c) {
  let max = a;
  if (b > max) {
    max = b;
  }
  if (c > max) {
    max = c;
  }
}
function findMaxEven(a, b, c) {
  let max = Number.NEGATIVE_INFINITY;

  if (a % 2 === 0 && a > max) {
    max = a;
  }
  if (b % 2 === 0 && b > max) {
    max = b;
  }
  if (c % 2 === 0 && c > max) {
    max = c;
  }
  return max;
}
console.log("findMaxEven: ", findMaxEven(4, 7, 2));
function extractTheTens(n) {
  if (n.toString().length !== 3) return -1;
  return Math.trunc((n % 100) / 10);
}
console.log("extractTheTens: ", extractTheTens(245));
function extractTheHundreds(n) {
  if (n.toString().length !== 3) return -1;
  return Math.trunc(n / 100);
}
console.log("extractTheHundreds: ", extractTheHundreds(245));
function sumDigist(n) {
  if (n.toString().length !== 3) return -1;
  const tens = extractTheTens(n);
  const hundreds = extractTheHundreds(n);
  const ones = n % 10;
  return tens + hundreds + ones;
}
console.log(sumDigist(112));
